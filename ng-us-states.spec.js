'use strict';

describe('Service: USStateService', function () {

    // load the service's module
    beforeEach(module('ngUSStates'));

    // instantiate service
    var USStateService;
    beforeEach(inject(function (_USStateService_) {
        USStateService = _USStateService_;
    }));

    it('USStateService should be defined', function () {
        expect(!!USStateService).toBe(true);
    });

    describe('When USStateService.getStates is called', function () {
        var results;

        beforeEach(function () {
          results = USStateService.getStates();
        });

        it('Should return 50 states', function () {
          expect(results.length).toBe(50);
        });
    });

    describe('When USStateService.getTerritories is called', function () {
        var results;

        beforeEach(function () {
            results = USStateService.getTerritories();
        });

        it('Should return 9 Territories', function () {
            expect(results.length).toBe(9);
        });
    }); 

    describe('When USStateService.getStatesAndTerritories is called', function () {
        var results;

        beforeEach(function () {
            results = USStateService.getStatesAndTerritories();
        });

        it('Should return 59 states and territories', function () {
            expect(results.length).toBe(59);
        });

        it('Should order all entities by name', function () {
            expect(results[0].name).toEqual('Alabama');
            expect(results[1].name).toEqual('Alaska');
            expect(results[2].name).toEqual('American Samoa');
        });
    });

    describe('When USStateService.getAll is called', function () {
        var results;

        beforeEach(function () {
            results = USStateService.getAll();
        });

        it('Should return 59 states and territories', function () {
            expect(results.length).toBe(59);
        });

        it('Should order all entities by name', function () {
            expect(results[0].name).toEqual('Alabama');
            expect(results[1].name).toEqual('Alaska');
            expect(results[2].name).toEqual('American Samoa');
        });
    });

});