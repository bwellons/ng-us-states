# README #

This module provides a simple Angular 1.x service for retrieving US States and Territories

### Methods ###

* getStates - returns 50 states
* getTerritories - returns 9 US Territories
* getStatesAndTerritories / getAll - returns alphabteized list of 59 States and Territories


### How do I get set up? ###

```bash
bower install ng-us-states --save
```
OR
```bash
npm install ng-us-states --save
```
### Eample Usage ###
```html
<html ng-app="app">
	<head>
		<script src="bower_components/angular/angular.js"></script>
		<script src="bower_components/ng-us-states/ng-us-states.js"></script>
		<script>
		angular.module('app', ['ngUSStates'])
			.controller(function (USStateService) {
				var states = USStateService.getAll();
			});	
		</script>
	</head>
</html>
```

### Contribution guidelines ###

* Write tests
* Submit Pull Request

### Who do I talk to? ###

* Brant Wellons <Brant@BrantWellons.com>