module.exports = function (config) {
	config.set({
		frameworks: ['jasmine'],
		browsers: ['PhantomJS'],
		files: [
			'bower_components/angular/angular.js',
			'bower_components/angular-mocks/angular-mocks.js',
			'bower_components/lodash/lodash.js',
			'ng-us-states.js',
			'ng-us-states.spec.js'
		],
		reporters: ['spec']
	});
};