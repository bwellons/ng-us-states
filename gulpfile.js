var gulp = require('gulp');
var karma = require('karma').server;

gulp.task('default', function () {
	gulp.start('test');
});

gulp.task('test', function () {
	karma.start({
		configFile: __dirname + '/karma.conf.js',
		singleRun: true
	}, function () {
		// done();
	});
});